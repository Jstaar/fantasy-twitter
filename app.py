import json
import os
from pprint import pprint

import oauth2 as oauth
from flask import Flask, request, session, g, redirect, url_for, abort, \
    render_template, flash


# Start application
app = Flask(__name__)

#Load twitter token
with open('config.json') as f:
    config = json.load(f)

consumer_key = config.get('consumer_key')
consumer_secret = config.get('consumer_secret')
access_token = config.get('access_token')
access_token_key = config.get('access_token_key')

consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)
access_token = oauth.Token(key=access_token, secret=access_token_key)
client = oauth.Client(consumer, access_token)

# pull from twitter api (looking for tweets with #fantasyfootball)
twitterquery = "https://api.twitter.com/1.1/search/tweets.json?q=%23fantasyfootball&src=tyah&count=100"

# get json from api twitter
response, data = client.request(twitterquery)


#Encodes the json into a string
#tweets = json.dumps(data)

#Decodes the json string into an object
tweets = json.loads(data)

#pretty print the json to better locate stuff
#pprint(tweets)

print '\n'
for element in tweets['statuses']:
    for key, value in element.iteritems():
        if 'text' in key:
            print value
            print '\n'


